# -*- coding: utf-8 -*-
"""
Created on:  Tue Feb  07 2023
Upgraded on: Sat Sep  09 2023

@authors: Blazhe Gjorgiev (gblazhe@ethz.ch)
          Joe Wengler
          Aknoledegments: https://vknight.org/blog/posts/continuous-time-markov-chains/
          
Objectives:
    1) Organize/agragte the transmission asset event data
    2) Calculate the steady state probabilities of transmission assets using Markov processes
"""

###############################################################################
# Import packages
import pandas as pd
import numpy as np
import scipy as sp
from scipy.integrate import odeint
import matplotlib.pyplot as plt


###############################################################################
# %% Paths and control flags
###############################################################################

# Data files path
file_name = 'Input/00_Transmission_Outage_statistics_preprocessed_ENTSO_E_data.csv'

# Agregate/organize the events acording to a given atribute
aggregate_cols = ['function']
# possible options (atributes):
#   - function
#   - productiontype
#   - mapcode1, mapcode2
#   - powerresourceeic
#   - singlecomponent1
#   - singlecomponent2
# NOTE: You can organize/aggregate the data based on given atribute, such as 
#       function (internal line, interconnector, transformer, substation), 
#       location (mapcode1, mapcode2), powerresourceeic (Grid component identifier)
#       singlecomponent1 (group all component types into one grid component)
#       singlecomponent2 (group all component types into one grid component, except Substation)
# Meaningful aggregations: ['function'], ['mapcode1', 'mapcode2'], ['powerresourceeic', 'mapcode1', 'mapcode2']

# Solver type for Markov
steady_type = 1
# 1 for Method 1: Least squares
# 2 for Method 2: Linear algebra
# 3 for Method 3: 0-eigenvalue
# 4 for Method 4: Solving the defining differential equation analyticaly
# 5 for Method 5: Solving the defining differential equation numerically

###############################################################################
# SET THE CLASS
###############################################################################
class Continiouse_Markov_Trans:
    ###############################################################################
    # %% Set the method
    ###############################################################################  
    def __init__(self):
 
        # Read data
        self.df     = pd.read_csv(file_name,encoding='latin-1')

    ###############################################################################
    # %% Calculate the steady state probabilities for given atribute (function
    #   - location (mapcode1, mapcode2)
    #   - powerresourceeic
    #   - singlecomponent1)
    # Write a csv file with all relevant parameters (transition rates, steady-state probabilities)
    ###############################################################################

    def calc_steady_state(self):
        """
        Calculats the steady-state probabilities of a 3-state tranmission asset model 
        using Markov processes. The computations are done for a given atribute (
        #   - function
        #   - location (mapcode1, mapcode2)
        #   - powerresourceeic
        #   - singlecomponent1)
                   
        Inputs
        -------
        df          the dataframe
        
        Outputs
        -------
        .csv        the organized dataframe, including the steady-state probabilities
    
        """
        
        # Read data
        df = self.df
        
        # Get an organized dataframe per given atribute
        agg_df = self.process_data(df)
        
        # For each group compute Markov
        for i in range(len(agg_df)):
            # Get element index
            elem = agg_df.iloc[i]
            
            # Set transition matrix and check Markov properties
            A, empty_states = self.check_markov(elem)
            
            # Run Markov processes
            P = self.calc_markov(A,empty_states)
            
            # Apend Markov results to the designated columns
            agg_df.at[i, 'P0'] = P[0]
            agg_df.at[i, 'P3'] = P[1]
            agg_df.at[i, 'P4'] = P[2]
            agg_df.at[i,'FOR'] = P[2]/(P[0]+P[2])
        
        # Update P0
        agg_df = agg_df[agg_df['P0'] != -1]
        
        ## Writh the datframe into csv
        agg_df.to_csv('Output/Transmission_Statistics_%s_Markov.csv'%('_'.join(aggregate_cols)),index=False)
        
        
    ###############################################################################
    # %% Process the event data
    ###############################################################################
    def process_data(self,df):
        """
        Process, organize, and group the generator failure data

        Inputs
        -------
        df          the dataframe
        
        Outputs
        -------
        agg_df      the organized dataframe

        """
        
        # %% Organize data
        
        # Organize Substation data
        df.loc[df['function'] == 'Busbar Coupler', 'productiontype'] = 'Substation'
        df.loc[df['function'] == 'Busbar Coupler, Transformer', 'productiontype'] = 'Substation'
        df.loc[df['productiontype'] == 'Substation', 'function'] = 'Substation'
        
        # Organize Internal line data
        df.loc[df['function'].isnull(), 'function'] = 'Internal Line'
        
        # Organize Transformer data
        df.loc[df['productiontype'] == 'Transformer', 'function'] = 'Transformer'
        
        # Standerize function names
        df.replace({'function': {'Corridor': 'Tieline', 'Tieline, Transformer': 'Tieline', 
                                 'Tieline, Internal Line': 'Internal Line', 
                                 'Internal Tie-Line': 'Internal Line', 
                                 'Internal Line, Tieline': 'Internal Line', 
                                 'Empty/No info': 'Internal Line', 'Busbar Coupler': 'Substation',
                                 'Busbar Coupler, Transformer': 'Substation', 
                                 'Transformer, Busbar Coupler': 'Substation'}}, inplace = True)
        
        
        # Agregate data acording to mapcode (location)
        if aggregate_cols == ['mapcode1', 'mapcode2']:
            df.loc[df['function']=='Tieline']
        
        # Add additional column for a single component (all components), to alow group summary
        if aggregate_cols[0] == 'singlecomponent1':
            df['singlecomponent1'] = 'Single'
        
        # Add additional column for a single component (all components, except substation), to alow group summary
        if aggregate_cols[0] == 'singlecomponent2':
            # Remove substation entries
            df = df.drop(df[df['productiontype'] == 'Substation'].index)
            # Add the column Single
            df['singlecomponent2'] = 'Single'
            
        # Calulate and add the operating hours
        df['ph'] = df['ah'] + df['foh'] + df['poh']
        
        # %% Count the events per atribute
        event_count = df.groupby(aggregate_cols).size().reset_index(name='No. Events')
            
        # %% Perform group sumary
        agg_df = df.groupby(aggregate_cols, as_index = False)[['ph', 'ah', 'poh', 'foh', 
                        'n_03', 'n_04', 'n_30', 'n_34', 'n_40', 'n_43','d_03', 'd_04', 'd_30', 'd_34', 'd_40', 'd_43']].sum()
        
        # %% Reorganize the dataframe
        if aggregate_cols == ['function']:
            agg_df.set_index('function', inplace = True)
            agg_df = agg_df.loc[['Internal Line', 'Tieline', 'Transformer', 'Substation']]
            agg_df.reset_index(inplace = True)
        
        # Merge the aggregated dataframe with the counts
        agg_df = pd.merge(agg_df, event_count, on=aggregate_cols)
        # Get column names
        columns = agg_df.columns.tolist()
        # Remove 'No. Events' from the list of columns
        columns.remove('No. Events')
        # Insert 'No. Events' at the desired position
        columns.insert(len(aggregate_cols), 'No. Events')
        # Reorder columns in DataFrame
        agg_df = agg_df[columns]
        
        # %% Calculate the tranistion rates
        # Calculate the tranistion rates from state 0 (available)
        agg_df['a03'] = (agg_df['n_03']/agg_df['ah']).fillna(0)
        agg_df['a04'] = (agg_df['n_04']/agg_df['ah']).fillna(0)
        agg_df['a00'] = (-agg_df['a03']-agg_df['a04']).fillna(0)
        # Move the the "ii" column to its correct position
        idx0 = agg_df.columns.get_loc('a03')
        col0 = agg_df.pop('a00')
        agg_df.insert(idx0, col0.name, col0)
        
        # Calculate the tranistion rates from state 3 (planned outage)
        agg_df['a30'] = (agg_df['n_30']/agg_df['poh']).fillna(0)
        agg_df['a34'] = (agg_df['n_34']/agg_df['poh']).fillna(0)
        agg_df['a33'] = (-agg_df['a30']-agg_df['a34']).fillna(0)
        # Move the the "ii" column to its correct position
        idx3 = agg_df.columns.get_loc('a34')
        col3 = agg_df.pop('a33')
        agg_df.insert(idx3, col3.name, col3)
        
        # Calculate the tranistion rates from state 4 (forced outage)
        agg_df['a40'] = (agg_df['n_40']/agg_df['foh']).fillna(0)
        agg_df['a43'] = (agg_df['n_43']/agg_df['foh']).fillna(0)
        agg_df['a44'] = (-agg_df['a40']-agg_df['a43']).fillna(0)
        
        # Set columns for the steady state probabilites
        agg_df['P0']  = 0.0 # Available
        agg_df['P3']  = 0.0 # Planned outage
        agg_df['P4']  = 0.0 # Forced outage
        agg_df['FOR'] = 0.0 # Forced outage rate (FOR)
        
        # Remove rows with infinities (occure due to incomplite data or very low number of entries)
        agg_df.replace([np.inf, -np.inf], np.nan, inplace=True) # Replace infinities with NaN
        agg_df = agg_df.dropna() # Drop rows with NaN values
        
        # Reset the index and drop the old index
        agg_df.reset_index(drop=True, inplace=True)

        # Save in constructor
        self.agg_df = agg_df
        
        return agg_df
    
    ###############################################################################
    # %% Check Markov properties
    ###############################################################################
    def check_markov(self,elem):
        """
        Check if transition matrix A fullfils desired properties 
           
        Inputs
        ----------
        elem         pandas dataframe that contains the number of transitions, 
                     the time between transitions, generator transition states data, etc.
        
        Outputs
        -------
        A            array containing the transition rates
        empty_states a list containg incomplite state transitions
        """
        #__________________________________________________________________________
        # %% Set the transition rate matrix A
        A = np.array([[elem['a00'], elem['a03'], elem['a04']],
                       [elem['a30'], elem['a33'], elem['a34']],
                       [elem['a40'], elem['a43'], elem['a44']]
                       ])
                   
        # %% Count the transitions into and out of the different states
        # Calculate the "into" and "out of" state transitions for state 3
        in_state_3 = elem['n_03'] + elem['n_43']
        out_state_3 = elem['n_30'] + elem['n_34']
        # Calculate the "into" and "out of" state transitions for state 4
        in_state_4 = elem['n_04'] + elem['n_34']
        out_state_4 = elem['n_40'] + elem['n_43']
        # NOTE: We want to make sure that if we enter a state, we also exit that 
        #       state, otherwise the Markov chain is not closed and we cannot apply 
        #       the current methodology
        
        # If we have a state where we only enter, or only exit, we print an error and exit.
        # If the aggregated data is big anough, this should not occur.
        empty_states = []
        if in_state_3 == 0 or out_state_3 == 0:
            A = np.delete(A, 1, 0)
            A = np.delete(A, 1, 1)
            empty_states.append(1)
            
        if in_state_4 == 0 or out_state_4 == 0:
            loc = 2-len(empty_states)
            A = np.delete(A, loc, 0)
            A = np.delete(A, loc, 1)
            empty_states.append(2)
        
        return A, empty_states
    
            
    ###############################################################################
    # Calculate the steady state probabilities of tranmission assets
    ###############################################################################
    def calc_markov(self,A,empty_states):
        """Calculats the steady-state probabilities of a 3-state transmission 
           asset model using continiouse Markov processes.
           In a steady-state we have P*A = P.
           where, 
           P is a vector of steady-state probabilities, P0, P3, P4
                P0 - Available
                P3 - Planned outage
                P4 - Forced outage
           A is the transition matrix of dimension 5x5
           
           We solve this problem using several different methods
           
        Inputs
        ----------
        A           transtion rate matrix
        
        Outputs
        -------
        P            vector containing the steady-state probabilities
        
        """

        # %% Preparatory functions  
        def is_steady_state(state, A):
            """
            Returns a boolean as to whether a given state is a steady 
            state of the Markov chain corresponding to the matrix A
            """
            return np.allclose((state @ A), 0)


        def adjust_A(A):
            """
            Given the transition rate matrix, prepare it for a algebraic solution
            """
            dim = A.shape[0]
            M = np.vstack((A.transpose()[:-1], np.ones(dim)))
            b = np.vstack((np.zeros((dim - 1, 1)), [1]))
            return M, b


        # %% Implement methods to solve the stead-state continiouse-time Markov

        ## Method 1: Least squares
        def steady_state_least_squares(A):
            """
            Obtain the steady-state as the vector that 
            gives the minimum of a least squares optimisation problem.
            
            Inputs:
            A - the transition rate matrix
            """
            M, b = adjust_A(A)
            pi, _, _, _ = np.linalg.lstsq(M, b, rcond=None)
            return pi.transpose()[0]

        #--------------------------------------------------------------------------
        ## Method 2: Linear algebra
        def steady_state_linear_algebra(A):
            """
            Obtain the steady state vector as the solution of a linear algebraic system.
            
            Inputs:
            A - the transition rate matrix
            """
            M, b = adjust_A(A)
            return np.linalg.solve(M, b).transpose()[0]

        #--------------------------------------------------------------------------
        ## Method 3: 0-eigenvalue
        def steady_state_eigenvectors(A):
            """
            Obtain the steady state vector as the eigenvector that corresponds to the eigenvalue 0.
            
            Inputs:
            A: the transition rate matrix
            """
            
            eigenvalues, eigenvectors = np.linalg.eig(A.T)

            zero_eigenvector = eigenvectors.transpose()[np.argmin(np.abs(eigenvalues))]
            
            P = zero_eigenvector / np.sum(zero_eigenvector)
            
            P = np.asarray(P).flatten()
            
            return P

        #--------------------------------------------------------------------------
        ## Method 4: Solving the defining differential equation analyticaly
        def steady_state_diffeq_analytical(A, max_t=100):
            """
            Solve the defining differential equation analytically until it converges.
            
            Inputs:
            A - the transition rate matrix
            max_t - the maximum time for which the differential equation is solved at each attempt.
            """
            
            # Initialize
            dimension = A.shape[0]
            P         = np.ones((1,dimension)) / dimension
            stateIter = P
            
            while not is_steady_state(P, A):
                P         = P @ sp.linalg.expm(A * max_t)
                stateIter = np.append(stateIter,P,axis=0) 
            
            P = np.asarray(P).flatten()
            
            return P, stateIter

        #--------------------------------------------------------------------------
        ## Method 5: Solving the defining differential equation numerically
        def steady_state_diffeq_numerical(A, max_t = 100, number_of_timepoints = 1000):
            
            """
            Solve the defining differential equation numericaly until it converges.
            
            Inputs:
            A - the transition rate matrix
            max_t - the maximum time for which the differential equation is solved
            number_of_time_points - the number of time points
            """
            
            def dx(x, t):
                return x @ A
            
            dimension = A.shape[0]
            P         = np.ones(dimension) / dimension
            stateIter = P.reshape(1,-1)
            
            while not is_steady_state(P, A):
                ts        = np.linspace(0, max_t, number_of_timepoints)
                xs        = odeint(func=dx, y0=P, t=ts)
                P         = xs[-1]
                tempState = P.reshape(1,-1)
                stateIter = np.append(stateIter,tempState,axis=0)
            
            P = np.asarray(P).flatten()
                
            return P, stateIter


        # %% Solve steady-state Markov
        if steady_type == 1:
            try:
                P = steady_state_least_squares(A)
            except:
                return [-1, -1, -1, -1, -1]    
        elif steady_type == 2:
            try:
                P = steady_state_linear_algebra(A)
            except:
                return [-1, -1, -1, -1, -1]
        elif steady_type == 3:
            try:
                P = steady_state_eigenvectors(A)
                
            except:
                return [-1, -1, -1, -1, -1]
        elif steady_type == 4:
            try:
                P, stateIter = steady_state_diffeq_analytical(A)
                dfDistrIter  = pd.DataFrame(stateIter)
                plt.show(dfDistrIter.plot())
            except:
                return [-1, -1, -1, -1, -1]
        else:
            try:
                P, stateIter = steady_state_diffeq_numerical(A)
                dfDistrIter = pd.DataFrame(stateIter)
                plt.show(dfDistrIter.plot())
            except:
                return [-1, -1, -1, -1, -1]
        
        # Check the empty states and corect the steady-state results (P) vector  
        j = 0
        P_new = np.zeros(3)
        for i in range(3):
            if i not in empty_states:
                P_new[i] = P[i-j]
            else:
                j += 1
        
        return P_new

###############################################################################        
# %% Execute the class
###############################################################################  
if __name__ == "__main__":
    # Instantiate the class and call the main method
    obj = Continiouse_Markov_Trans()
    obj.calc_steady_state()