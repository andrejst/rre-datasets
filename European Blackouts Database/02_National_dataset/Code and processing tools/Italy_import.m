function ItalyFinal = importfile1(workbookFile, sheetName, dataLines)
%IMPORTFILE1 Import data from a spreadsheet
%  ITALYFINAL = IMPORTFILE1(FILE) reads data from the first worksheet in
%  the Microsoft Excel spreadsheet file named FILE.  Returns the data as
%  a table.
%
%  ITALYFINAL = IMPORTFILE1(FILE, SHEET) reads from the specified
%  worksheet.
%
%  ITALYFINAL = IMPORTFILE1(FILE, SHEET, DATALINES) reads from the
%  specified worksheet for the specified row interval(s). Specify
%  DATALINES as a positive scalar integer or a N-by-2 array of positive
%  scalar integers for dis-contiguous row intervals.
%
%  Example:
%  ItalyFinal = importfile1("C:\Users\andrejst\Documents\Data analysis\Publication 2 - Database work\Italy_Final.xlsx", "Sheet1", [2, 27285]);
%
%  See also READTABLE.
%
% Auto-generated by MATLAB on 19-Aug-2022 11:33:50

%% Input handling

% If no sheet is specified, read first sheet
if nargin == 1 || isempty(sheetName)
    sheetName = 1;
end

% If row start and end points are not specified, define defaults
if nargin <= 2
    dataLines = [2, 27285];
end

%% Set up the Import Options and import the data
opts = spreadsheetImportOptions("NumVariables", 64);

% Specify sheet and range
opts.Sheet = sheetName;
opts.DataRange = "A" + dataLines(1, 1) + ":BL" + dataLines(1, 2);

% Specify column names and types
opts.VariableNames = ["nDisalimentazioneNPowerOutage", "UniqueEventIdentifier", "IsolatedEventCommonCauseCascadeSuspect", "SingleCommonCauseOrRelated", "Type", "SameEventDifferentCause", "nSRDRelatedToWhichEventFailsGoesUpToTheNumberOfFailuresInThisEv", "AccettazioneUtenteUserAcceptance", "StatoState", "Area", "DataggmmaaaaDateddmmyyyy", "Year", "Month", "Day", "Time", "OrahhmmssTimehhmmss", "Timestamp", "DescrizioneElementoDiReteOrigineDellaDisalimentazioneNetworkEle", "NetworkElementDescriptionSourceOfThePowerOutage", "DescrizioneComponenteDiReteOrigineDellaDisalimentazioneSourceNe", "SourceNetworkComponentDescriptionOfThePowerOutage", "TitolareReteOrigineDellaDisalimentazioneNetworkOwnerOfTheSource", "TensioneReteOrigineDellaDisalimentazionekVSourceMainVoltageOfPo", "CodiceCausaAEEG1LivelloAEEGARERAInstitutionAEEGCauseCode1stLeve", "Code1Meaning", "CodiceCausaAEEG2LivelloAEEGCauseCode2ndLevel", "Code2Meaning", "CodiceCausaAEEG3LivelloAEEGCauseCode3rdLevel", "MainCause", "TipoInterruzioneTypeOfInteruptionL3minB3min1secT1sec", "ConfigurazioneReteAllistanteDellaDisalimentazioneNetworkConfigu", "NetworkConfigurationAtPowerOutageMoment", "NomeDelSitoUtenteATDisalimentatoSiteNameOfTheDisconnectedHVUser", "CodiceUnivocoDelSitoUtenteATUniqueCodeOfTheHVUserSite", "ConnessioneAllaRTNConnectionToTheNTGnationalTransmissionGrid", "ConnectionToNTGFSIRailwayGrid", "TipologiaDiConnessioneConnectionType", "ConnectionType", "TensioneSitoUtenteATkVHighVoltageUserSiteVoltagekV", "TitolareDelSitoUtenteATOwnerOfTheHighVoltageUserSite", "PIMWIterruptedPowerMW", "DurataDisalimentazioneATmmssLengthtimeOfHVOutage", "ENSENRLordaMWhEnergiaNonSollecitataEnrgiaNonRichiestaGrossEnerg", "ENSENRNettaMWhNetEnergyNotSuppliedNotWithdrawnMWh", "NFNRSDNonFornitaNonRichiestaSenzaDatiNotDeliveredNotRequestedNA", "NIncidenteRilevanteNRelevantAccident", "NPotenzialeIncidenteRilevanteNPotentiallyRelevantAccident", "DurataMTBTmmssMVmediumVoltageLVlowVoltageDurationmmss", "Durations", "IndennizzoAClienteFinaleAATATCompensationToAATATFinalCustomer", "FEEFundExceptionalEvents", "MITMitigationIndicatesSIInCaseOfMitigationCarriedOutByThePartyO", "PICaricoMWPILoadMW", "PIProduzioneMWPIProductionMW", "ENSUsataLordaMWhENSUsedGrossMW", "ENSUNettaMWhENSUsedNetMW", "ENRULordaMWhENRUsedGrossMW", "ENRUNettaMWhENRUsedNetMWh", "CodiceSDISistemaDiInterscambioSDICodeExchangeSystemRealtedToBil", "CodiceASPASPCodePositionRelated", "VarName61", "DurationSec", "DurationMin", "EnergyLoss"];
opts.VariableTypes = ["double", "string", "categorical", "categorical", "categorical", "categorical", "string", "categorical", "categorical", "categorical", "categorical", "double", "double", "double", "string", "string", "datetime", "categorical", "categorical", "categorical", "categorical", "categorical", "double", "categorical", "categorical", "categorical", "string", "double", "categorical", "categorical", "categorical", "categorical", "categorical", "categorical", "categorical", "categorical", "categorical", "categorical", "double", "categorical", "double", "double", "double", "string", "categorical", "string", "string", "double", "double", "string", "categorical", "categorical", "double", "double", "double", "string", "double", "string", "string", "categorical", "string", "double", "double", "double"];

% Specify variable properties
opts = setvaropts(opts, ["UniqueEventIdentifier", "nSRDRelatedToWhichEventFailsGoesUpToTheNumberOfFailuresInThisEv", "Time", "OrahhmmssTimehhmmss", "Code2Meaning", "ENSENRNettaMWhNetEnergyNotSuppliedNotWithdrawnMWh", "NIncidenteRilevanteNRelevantAccident", "NPotenzialeIncidenteRilevanteNPotentiallyRelevantAccident", "IndennizzoAClienteFinaleAATATCompensationToAATATFinalCustomer", "ENSUNettaMWhENSUsedNetMW", "ENRUNettaMWhENRUsedNetMWh", "CodiceSDISistemaDiInterscambioSDICodeExchangeSystemRealtedToBil", "VarName61"], "WhitespaceRule", "preserve");
opts = setvaropts(opts, ["UniqueEventIdentifier", "IsolatedEventCommonCauseCascadeSuspect", "SingleCommonCauseOrRelated", "Type", "SameEventDifferentCause", "nSRDRelatedToWhichEventFailsGoesUpToTheNumberOfFailuresInThisEv", "AccettazioneUtenteUserAcceptance", "StatoState", "Area", "DataggmmaaaaDateddmmyyyy", "Time", "OrahhmmssTimehhmmss", "DescrizioneElementoDiReteOrigineDellaDisalimentazioneNetworkEle", "NetworkElementDescriptionSourceOfThePowerOutage", "DescrizioneComponenteDiReteOrigineDellaDisalimentazioneSourceNe", "SourceNetworkComponentDescriptionOfThePowerOutage", "TitolareReteOrigineDellaDisalimentazioneNetworkOwnerOfTheSource", "CodiceCausaAEEG1LivelloAEEGARERAInstitutionAEEGCauseCode1stLeve", "Code1Meaning", "CodiceCausaAEEG2LivelloAEEGCauseCode2ndLevel", "Code2Meaning", "MainCause", "TipoInterruzioneTypeOfInteruptionL3minB3min1secT1sec", "ConfigurazioneReteAllistanteDellaDisalimentazioneNetworkConfigu", "NetworkConfigurationAtPowerOutageMoment", "NomeDelSitoUtenteATDisalimentatoSiteNameOfTheDisconnectedHVUser", "CodiceUnivocoDelSitoUtenteATUniqueCodeOfTheHVUserSite", "ConnessioneAllaRTNConnectionToTheNTGnationalTransmissionGrid", "ConnectionToNTGFSIRailwayGrid", "TipologiaDiConnessioneConnectionType", "ConnectionType", "TitolareDelSitoUtenteATOwnerOfTheHighVoltageUserSite", "ENSENRNettaMWhNetEnergyNotSuppliedNotWithdrawnMWh", "NFNRSDNonFornitaNonRichiestaSenzaDatiNotDeliveredNotRequestedNA", "NIncidenteRilevanteNRelevantAccident", "NPotenzialeIncidenteRilevanteNPotentiallyRelevantAccident", "IndennizzoAClienteFinaleAATATCompensationToAATATFinalCustomer", "FEEFundExceptionalEvents", "MITMitigationIndicatesSIInCaseOfMitigationCarriedOutByThePartyO", "ENSUNettaMWhENSUsedNetMW", "ENRUNettaMWhENRUsedNetMWh", "CodiceSDISistemaDiInterscambioSDICodeExchangeSystemRealtedToBil", "CodiceASPASPCodePositionRelated", "VarName61"], "EmptyFieldRule", "auto");
opts = setvaropts(opts, "Timestamp", "InputFormat", "");

% Import the data
ItalyFinal = readtable(workbookFile, opts, "UseExcel", false);

for idx = 2:size(dataLines, 1)
    opts.DataRange = "A" + dataLines(idx, 1) + ":BL" + dataLines(idx, 2);
    tb = readtable(workbookFile, opts, "UseExcel", false);
    ItalyFinal = [ItalyFinal; tb]; %#ok<AGROW>
end

end