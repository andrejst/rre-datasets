# RRE Datasets
The Reliability and Risk Engineering Laboratory (RRE) at the Department of Mechanical and Process Engineering (D-MAVT) at ETH Zurich, presents the RRE Datasets.

The datasets are a collection of events and failures, used for analyses of blackout events and component availability. The compilation is comprised of:
- Availability of European Power Grid Assets datasets
- European Blackouts database

The data in this work is collected from multiple open sources (see descriptions in the respective folders) and is free to be used by researchers, operators and all interested parties.

## Availability of European Power Grid Assets
<b>NOTE: The Availability datasets are being updated and will not be available until the middle of September, 2024.

This collection of datasets presents performance indicators for generating units and transmission grid assets in the European power system. We use generator and transmission unavailability data from the ENTSO-E transparency platform (https://transparency.entsoe.eu/) and quantify plant-specific availabilities of the European power system key assets. To this aim, we extract relevant asset outage events and calculate transition rates. Subsequently, we apply Markov processes to the compute steady-state probabilities of generators per type and size, internal lines, interconnects, transformers, and substations. 

The directory is comprised of three folders:
- 01_Transmission_data - availability of transmission grid assets, including: transmission lines, interconnectors, transformers and substations.
- 02_Generation_data   - availability of generating units, including: nuclear, coal, gas, hydro and renewable units.
- 03_Codes_Markov      - the code used to calculate the steady-state probabilities.
- Documentation        - a document that explains the content of each dataset.

For more information regarding the results and data processing, please read the associated publication (pending submission).

<b>Authors and acknowledgment:

Blazhe Gjorgiev,
Andrej Stankovski,
Joé Wengler,
Sinan Sencan and 
Giovanni Sansavini

<b>How to cite:
<tr>Gjorgiev, B., Stankovski, A., Wengler, J., Sencan, S., & Sansavini, G. (2025). Availability of the European power system assets: What we learn from data?. Reliability Engineering & System Safety. https://doi.org/10.1016/j.ress.2025.110887

## European Blackouts Database
This database contains blackout events occurring the European power transmission system. The events in the datasets have been meticulously analyzed and classified, making this the largest public curated database of events from the European transmission system.

The database is comprised of two datasets:
- 01_Continental_dataset - contains 478 severe events from the European power transmission system, collected by our team.
- 02_National_dataset    - contains 20'980 granular failures from the Italian transmission system, published by the system operator TERNA.
- Documentation          - a document that explains the content of each dataset.

For more information regarding the results and data processing, please read the associated publication.

<b>Authors and acknowledgment:

Andrej Stankovski,
Blazhe Gjorgiev,
Leon Locher and 
Giovanni Sansavini


Contribution: 
Falko Chmiel and
Simon Hässig

<b>How to cite:
<tr>Stankovski, A., Gjorgiev, B., Locher, L., and Sansavini, G. (2023). Power blackouts in Europe: Analyses, key insights, and recommendations from empirical evidence. Joule. 10.1016/j.joule.2023.09.005.

## Disparate power transmission performance reinforces Italian social inequities
This database contains failure events occurring in Italian high-voltage electricity grid 2013-2022 and affecting electricity-utility consumers. The raw input data is based on national-level failure data from the European Blackouts Database as well as open data from Eurostat, Istat, and the Italian national transmission system operator. 

The database is comprised of three datasets:
- 01_Failure_Events.xlsx                - contains 11,831 failure events from the Italian power transmission system.
- 02_Weather_Averages_NUTS2.xlsx        - contains the weather averages for each Italian region 2013-2022.
- 03_CVI.xlsx                           - contains input data for Community Vulnerability Index (CVI) and CVI values per region.
- 04_IVI.xlsx                           - contains input data for Infrastructure Vulnerability Index (IVI) and IVI values per region.
- 05_All_Indices.xlsx                   - contains sumary vulnerability indices per region.
- 06_TSO_Substations.xlsx               - contains a raw list of substations connected to the Italian high-voltage grid.
- Documentation.pdf                     - a document that explains the content of each dataset.

For more information regarding the results and data processing, please refer to the associated publication and complementary Supplementary Information.

<b>Authors and acknowledgment:

Katherine Emma Lonergan,
Andrej Stankovski,
Blazhe Gjorgiev and 
Giovanni Sansavini


Contributors: 
Fatima Sarfraz, 
Fiona Kriwan and
Francesco De Marco


<b>How to cite:
<tr>Lonergan, K.E., Stankovski, A., Gjorgiev, B., and Sansavini, G. (2024). Disparate power transmission performance reinforces Italian social inequities. Working paper. 

## Contact
Andrej Stankovski (andrejst@ethz.ch)

Blazhe Gjorgiev (gblazhe@ethz.ch)

Giovanni Sansavini (sansavig@ethz.ch)


## Disclaimer
The data presented here is compiled and processed to the best of the author's knowledge. The authors do not bear responsibility for the use of this data. 

Please exercise caution when using this data for scientific research, and inform the authors if you uncover any innaccuracies.

The code and processing tools presented in this work are still a work in progress and part of larger studies conducted at the RRE. 
Therefore, the code might not be intuitive or fully usable for the public at the current time. The code will be updated and properly documented in the coming weeks.
