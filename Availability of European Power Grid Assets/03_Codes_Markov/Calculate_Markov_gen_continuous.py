# -*- coding: utf-8 -*-
"""
Created on:  07. 02.2023
Upgraded on: 08. 09. 2023
Major upgrade and restructuring: 14.05.2024 

@authors: Blazhe Gjorgiev (gblazhe@ethz.ch)
          Joe Wengler
          Acknowledgments: https://vknight.org/blog/posts/continuous-time-markov-chains/
          
Objectives:
    1) Organize/aggregate the generator event data and compute the transition rates
    2) Calculate the steady-state probabilities of generators using continuous-time Markov processes
"""

###############################################################################
# Import packages
import pandas as pd
import numpy as np
import scipy as sp
from scipy.integrate import odeint
import matplotlib.pyplot as plt

###############################################################################
# Paths and control flags
###############################################################################
# Data file paths
file_name = 'Input/00_Generators_Outage_statistics_preprocessed_ENTSO_E_data.csv'  # Generator statistics
age_file  = 'Input/00_Generator_information_hourly_ENTSO_E.csv'                    # Generator info - age, capacity

# Agregate/organize the events according to a given attribute
aggregate_cols = ['productiontype_group']
# possible options (attributes):
#   - productiontype
#   - productiontype_group
#   - mapcode
#   - capacity_level
#   - powerresourceeic
#   - age
#   - singlecomponent
# NOTE: You can organize/aggregate the data based on a given attribute, such as 
#       production type, location, capacity
# NOTE: You can select 'single component' to aggragte all data into a single 
#       generator component. This could be useful in case you use only a single 
#       failure probability value.
# NOTE: Meaningful aggregations: ['productiontype'], ['mapcode', 'productiontype_group'], ['productiontype', 'capacity_level']

# Weight the generator parameters and thus the transition rates
weighted_markov = True
# True  to use weighted
# False to use unweighted
# NOTE: The deafult and recommended option is the weighted aggregation

# Solver type for Markov
steady_type = 1
# 1 for Method 1: Least squares
# 2 for Method 2: Linear algebra
# 3 for Method 3: 0-eigenvalue
# 4 for Method 4: Solving the defining differential equation analytically
# 5 for Method 5: Solving the defining differential equation numerically

###############################################################################
# SET THE CLASS
###############################################################################
class Continiouse_Markov_Gen:

    ###############################################################################
    # %% Set the method
    ###############################################################################  
    def __init__(self):
 
        # Read data
        self.df     = pd.read_csv(file_name,encoding='latin-1')
        self.age_df = pd.read_csv(age_file)
    
        
    ###############################################################################
    # %% Calculate the steady-state probabilities for the given attribute (productiontype
    #   - productiontype_group
    #   - mapcode
    #   - capacity_level)
    # Write a csv file with all relevant parameters (transition rates, steady-state probabilities)
    ###############################################################################
    def calc_steady_state(self):
        """
        Calculate the steady-state probabilities for a 5-state generator model
        using Markov. The computations are done for a given attribute (
        #   - productiontype
        #   - productiontype_group
        #   - mapcode
        #   - capacity_level, etc.)

        Inputs
        -------
        df          the dataframe
        
        Outputs
        -------
        .csv        the organized dataframe, including the steady-state probabilities

        """
        # Read data
        df = self.df
        
        # Get an organized dataframe per given attribute
        agg_df = self.process_data(df)
        
        # For each group compute Markov
        for i in range(len(agg_df)):
            # Get element index
            elem = agg_df.iloc[i]
            
            # Set transition matrix and check Markov properties
            A, empty_states = self.check_markov(elem)
            
            # Run Markov processes
            x = self.calc_markov(A,empty_states)
            
            # Apend Markov results to the designated columns
            agg_df.at[i, 'P0'] = x[0]
            agg_df.at[i, 'P1'] = x[1]
            agg_df.at[i, 'P2'] = x[2]
            agg_df.at[i, 'P3'] = x[3]
            agg_df.at[i, 'P4'] = x[4]
            agg_df.at[i,'FOR'] = x[4]/(x[0]+x[4])
        
        # Update P0
        agg_df = agg_df[agg_df['P0'] != -1]
        
        
        ## Write the datframe into csv
        if weighted_markov:
            agg_df.to_csv('Output/Generator_Statistics_%s_Markov_weighted.csv'%('_'.join(aggregate_cols)),index=False)
        else:
            agg_df.to_csv('Output/Generator_Statistics_%s_Markov.csv'%('_'.join(aggregate_cols)),index=False)
    
            
    ###############################################################################
    # %% Process the event data
    ###############################################################################    
    def process_data(self,df):
        """
        Process, organize, and group the generator failure data

        Inputs
        -------
        df          the dataframe
        
        Outputs
        -------
        agg_df      the organized dataframe

        """
        
        # %% Organize the data

        # Drop outliers
        df = df[df['powerresourceeic'] != '17W100P100P03655']
        df = df[df['powerresourceeic'] != '15WGGUTPVAE--PVH']
        
        # Set capacity levels
        df['capacity_level'] = np.where(df['installedcapacity']<50, 50, 
                               np.where(df['installedcapacity']<100, 100, 
                               np.where(df['installedcapacity']<250, 250,
                               np.where(df['installedcapacity']<500, 500,
                               np.where(df['installedcapacity']<1000, 1000, 10000)))))
        
        # Organize data acording to production type
        df.loc[df['productiontype'] == 'Biomass', 'productiontype_group'] = 'Fossil'
        df.loc[df['productiontype'] == 'Fossil Brown coal/Lignite', 'productiontype_group'] = 'Fossil'
        df.loc[df['productiontype'] == 'Fossil Coal-derived gas', 'productiontype_group'] = 'Fossil'
        df.loc[df['productiontype'] == 'Fossil Gas', 'productiontype_group'] = 'Fossil'
        df.loc[df['productiontype'] == 'Fossil Hard coal', 'productiontype_group'] = 'Fossil'
        df.loc[df['productiontype'] == 'Fossil Oil', 'productiontype_group'] = 'Fossil'
        df.loc[df['productiontype'] == 'Fossil Oil shale', 'productiontype_group'] = 'Fossil'
        df.loc[df['productiontype'] == 'Fossil Peat', 'productiontype_group'] = 'Fossil'
        df.loc[df['productiontype'] == 'Waste', 'productiontype_group'] = 'Fossil'
        #df.loc[df['productiontype'] == 'Geothermal', 'productiontype_group'] = 'Other'
        df.loc[df['productiontype'] == 'Other', 'productiontype_group'] = 'Other'
        df.loc[df['productiontype'] == 'Hydro Pumped Storage', 'productiontype_group'] = 'Hydro'
        df.loc[df['productiontype'] == 'Hydro Run-of-river and poundage', 'productiontype_group'] = 'Hydro'
        df.loc[df['productiontype'] == 'Hydro Water Reservoir', 'productiontype_group'] = 'Hydro'
        df.loc[df['productiontype'] == 'Nuclear', 'productiontype_group'] = 'Nuclear'
        df.loc[df['productiontype'] == 'Solar', 'productiontype_group'] = 'Solar'
        df.loc[df['productiontype'] == 'Wind Offshore', 'productiontype_group'] = 'Wind'
        df.loc[df['productiontype'] == 'Wind Onshore', 'productiontype_group'] = 'Wind'
                
        # Agregate data acording to age of generators
        if 'age' in aggregate_cols:
            age_df = self.age_df
            age_df = age_df[['powerresourceeic','commissioned']].copy()
            #age_df.rename(columns={'commissioned': 'age'}, inplace = True)
            age_df = age_df[age_df['commissioned'].notna()]
            age_df = age_df[age_df['commissioned']<2022]
            df = df.merge(age_df, how = 'inner', on = 'powerresourceeic')
            df['age'] = df['year'] - df['commissioned']
        
        # Add additional column for a single component, to alow group summary
        if aggregate_cols[0] == 'singlecomponent':
            df['singlecomponent'] = 'Single'
            
        # Calulate and add the period hours (per month)
        df['ph'] = df['ah'] + df['fdh'] + df['pdh'] + df['foh'] + df['poh']
        
        # %% Count the events per atribute
        event_count = df.groupby(aggregate_cols).size().reset_index(name='No. Events')

        # %% Compute and add the weighted parameters in the dataframe
        if weighted_markov == True:
            df['wph']   = df['installedcapacity']*df['ph']
            df['wfdh']  = df['installedcapacity']*df['fdh']
            df['wpdh']  = df['installedcapacity']*df['pdh']
            df['wfoh']  = df['installedcapacity']*df['foh']
            df['wpoh']  = df['installedcapacity']*df['poh']
            df['wn_01'] = df['installedcapacity']*df['n_01']
            df['wn_02'] = df['installedcapacity']*df['n_02']
            df['wn_03'] = df['installedcapacity']*df['n_03']
            df['wn_04'] = df['installedcapacity']*df['n_04']
            df['wn_10'] = df['installedcapacity']*df['n_10']
            df['wn_12'] = df['installedcapacity']*df['n_12']
            df['wn_13'] = df['installedcapacity']*df['n_13']
            df['wn_14'] = df['installedcapacity']*df['n_14']
            df['wn_20'] = df['installedcapacity']*df['n_20']
            df['wn_21'] = df['installedcapacity']*df['n_21']
            df['wn_23'] = df['installedcapacity']*df['n_23']
            df['wn_24'] = df['installedcapacity']*df['n_24']
            df['wn_30'] = df['installedcapacity']*df['n_30']
            df['wn_31'] = df['installedcapacity']*df['n_31']
            df['wn_32'] = df['installedcapacity']*df['n_32']
            df['wn_34'] = df['installedcapacity']*df['n_34']
            df['wn_40'] = df['installedcapacity']*df['n_40']
            df['wn_41'] = df['installedcapacity']*df['n_41']
            df['wn_42'] = df['installedcapacity']*df['n_42']
            df['wn_43'] = df['installedcapacity']*df['n_43']        
            df['wd_01'] = df['installedcapacity']*df['d_01']
            df['wd_02'] = df['installedcapacity']*df['d_02']
            df['wd_03'] = df['installedcapacity']*df['d_03']
            df['wd_04'] = df['installedcapacity']*df['d_04']
            df['wd_10'] = df['installedcapacity']*df['d_10']
            df['wd_12'] = df['installedcapacity']*df['d_12']
            df['wd_13'] = df['installedcapacity']*df['d_13']
            df['wd_14'] = df['installedcapacity']*df['d_14']
            df['wd_20'] = df['installedcapacity']*df['d_20']
            df['wd_21'] = df['installedcapacity']*df['d_21']
            df['wd_23'] = df['installedcapacity']*df['d_23']
            df['wd_24'] = df['installedcapacity']*df['d_24']
            df['wd_30'] = df['installedcapacity']*df['d_30']
            df['wd_31'] = df['installedcapacity']*df['d_31']
            df['wd_32'] = df['installedcapacity']*df['d_32']
            df['wd_34'] = df['installedcapacity']*df['d_34']
            df['wd_40'] = df['installedcapacity']*df['d_40']
            df['wd_41'] = df['installedcapacity']*df['d_41']
            df['wd_42'] = df['installedcapacity']*df['d_42']
            df['wd_43'] = df['installedcapacity']*df['d_43']
            
            # Calulate and add the operating hours
            df['wah'] = df['wph'] - df['wfdh'] - df['wpdh'] - df['wfoh'] - df['wpoh']
            add_char = 'w'
            
            # Perform group summary
            agg_df = df.groupby(aggregate_cols, as_index = False)[['ph', 'ah', 'pdh', 'fdh', 'poh', 'foh',  
                            'n_01', 'n_02', 'n_03', 'n_04', 'n_10', 'n_12', 'n_13', 'n_14', 'n_20', 
                            'n_21', 'n_23', 'n_24', 'n_30', 'n_31', 'n_32', 'n_34', 'n_40', 'n_41', 
                            'n_42', 'n_43', 'd_01', 'd_02', 'd_03', 'd_04', 'd_10', 'd_12', 'd_13', 
                            'd_14', 'd_20', 'd_21', 'd_23', 'd_24', 'd_30', 'd_31', 'd_32', 'd_34', 
                            'd_40', 'd_41', 'd_42', 'd_43','wph', 'wfdh', 'wpdh', 'wfoh', 'wpoh', 'wah',
                            'wn_01', 'wn_02', 'wn_03', 'wn_04', 'wn_10', 'wn_12', 'wn_13', 'wn_14', 'wn_20', 
                            'wn_21', 'wn_23', 'wn_24', 'wn_30', 'wn_31', 'wn_32', 'wn_34', 'wn_40', 'wn_41', 
                            'wn_42', 'wn_43','wd_01', 'wd_02', 'wd_03', 'wd_04', 'wd_10', 'wd_12', 'wd_13', 
                            'wd_14', 'wd_20', 'wd_21', 'wd_23', 'wd_24', 'wd_30', 'wd_31', 'wd_32', 'wd_34', 
                            'wd_40', 'wd_41', 'wd_42', 'wd_43']].sum()
            
        # Organize the dataframe without weighted parameters
        else:
            # Perform group sumary
            agg_df = df.groupby(aggregate_cols, as_index = False)[['ph', 'ah', 'pdh', 'fdh', 'poh', 'foh',
                            'n_01', 'n_02', 'n_03', 'n_04', 'n_10', 'n_12', 'n_13', 'n_14', 'n_20', 
                            'n_21', 'n_23', 'n_24', 'n_30', 'n_31', 'n_32', 'n_34', 'n_40', 'n_41', 
                            'n_42', 'n_43','d_01', 'd_02', 'd_03', 'd_04', 'd_10', 'd_12', 'd_13', 
                            'd_14', 'd_20', 'd_21', 'd_23', 'd_24', 'd_30', 'd_31', 'd_32', 'd_34', 
                            'd_40', 'd_41', 'd_42', 'd_43']].sum()
            add_char = ''
            
        # Move production type Other at the end of the dataframe
        if  aggregate_cols[0] == 'productiontype' or aggregate_cols[0] == 'productiontype_group':
            # Row index
            if  aggregate_cols[0] == 'productiontype':
                idx_other = agg_df.index[agg_df['productiontype'] == 'Other'].tolist()
            else:
                idx_other = agg_df.index[agg_df['productiontype_group'] == 'Other'].tolist()
            # Get the two to shift
            row_to_shift = agg_df.loc[idx_other]
            # Drop the row from its original position
            agg_df = agg_df.drop(idx_other)
            # Append the row to the end
            agg_df = pd.concat([agg_df,row_to_shift],ignore_index=True)
            # Reset the index of the DataFrame
            agg_df = agg_df.reset_index(drop=True)
        
        # Merge the aggregated dataframe with the counts
        agg_df = pd.merge(agg_df, event_count, on=aggregate_cols)
        # Get column names
        columns = agg_df.columns.tolist()
        # Remove 'No. Events' from the list of columns
        columns.remove('No. Events')
        # Insert 'No. Events' at the desired position
        columns.insert(len(aggregate_cols), 'No. Events')
        # Reorder columns in DataFrame
        agg_df = agg_df[columns]
        
        # %% Calculate the tranistion rates
        # Calculate the tranistion rates from state 0 (available)
        agg_df['a01'] = (agg_df[add_char+'n_01']/agg_df[add_char+'ah']).fillna(0)
        agg_df['a02'] = (agg_df[add_char+'n_02']/agg_df[add_char+'ah']).fillna(0)
        agg_df['a03'] = (agg_df[add_char+'n_03']/agg_df[add_char+'ah']).fillna(0)
        agg_df['a04'] = (agg_df[add_char+'n_04']/agg_df[add_char+'ah']).fillna(0)
        agg_df['a00'] = (-agg_df['a01']-agg_df['a02']-agg_df['a03']-agg_df['a04']).fillna(0)
        # Move the the "ii" column to its correct position
        idx0 = agg_df.columns.get_loc('a01')
        col0 = agg_df.pop('a00')
        agg_df.insert(idx0, col0.name, col0)
        
        # Calculate the tranistion rates from state 1 (planned derated)
        agg_df['a10'] = (agg_df[add_char+'n_10']/agg_df[add_char+'pdh']).fillna(0)
        agg_df['a12'] = (agg_df[add_char+'n_12']/agg_df[add_char+'pdh']).fillna(0)
        agg_df['a13'] = (agg_df[add_char+'n_13']/agg_df[add_char+'pdh']).fillna(0)
        agg_df['a14'] = (agg_df[add_char+'n_14']/agg_df[add_char+'pdh']).fillna(0)
        agg_df['a11'] = (-agg_df['a10']-agg_df['a12']-agg_df['a13']-agg_df['a14']).fillna(0)
        # Move the the "ii" column to its correct position
        idx1 = agg_df.columns.get_loc('a12')
        col1 = agg_df.pop('a11')
        agg_df.insert(idx1, col1.name, col1)
        
        # Calculate the tranistion rates from state 2 (forced derated)
        agg_df['a20'] = (agg_df[add_char+'n_20']/agg_df[add_char+'fdh']).fillna(0)
        agg_df['a21'] = (agg_df[add_char+'n_21']/agg_df[add_char+'fdh']).fillna(0)
        agg_df['a23'] = (agg_df[add_char+'n_23']/agg_df[add_char+'fdh']).fillna(0)
        agg_df['a24'] = (agg_df[add_char+'n_24']/agg_df[add_char+'fdh']).fillna(0)
        agg_df['a22'] = (-agg_df['a20']-agg_df['a21']-agg_df['a23']-agg_df['a24']).fillna(0)
        # Move the the "ii" column to its correct position
        idx2 = agg_df.columns.get_loc('a23')
        col2 = agg_df.pop('a22')
        agg_df.insert(idx2, col2.name, col2)
        
        # Calculate the tranistion rates from state 3 (planned outage)
        agg_df['a30'] = (agg_df[add_char+'n_30']/agg_df[add_char+'poh']).fillna(0)
        agg_df['a31'] = (agg_df[add_char+'n_31']/agg_df[add_char+'poh']).fillna(0)
        agg_df['a32'] = (agg_df[add_char+'n_32']/agg_df[add_char+'poh']).fillna(0)
        agg_df['a34'] = (agg_df[add_char+'n_34']/agg_df[add_char+'poh']).fillna(0)
        agg_df['a33'] = (-agg_df['a30']-agg_df['a31']-agg_df['a32']-agg_df['a34']).fillna(0)
        # Move the the "ii" column to its correct position
        idx3 = agg_df.columns.get_loc('a34')
        col3 = agg_df.pop('a33')
        agg_df.insert(idx3, col3.name, col3)
        
        # Calculate the tranistion rates from state 4 (forced outage)
        agg_df['a40'] = (agg_df[add_char+'n_40']/agg_df[add_char+'foh']).fillna(0)
        agg_df['a41'] = (agg_df[add_char+'n_41']/agg_df[add_char+'foh']).fillna(0)
        agg_df['a42'] = (agg_df[add_char+'n_42']/agg_df[add_char+'foh']).fillna(0)
        agg_df['a43'] = (agg_df[add_char+'n_43']/agg_df[add_char+'foh']).fillna(0)
        agg_df['a44'] = (-agg_df['a40']-agg_df['a41']-agg_df['a42']-agg_df['a43']).fillna(0)
        
        # Set columns for the steady state probabilites
        agg_df['P0']  = 0.0 # Available
        agg_df['P1']  = 0.0 # Planned derated
        agg_df['P2']  = 0.0 # Forced derated
        agg_df['P3']  = 0.0 # Planned outage
        agg_df['P4']  = 0.0 # Forced outage
        agg_df['FOR'] = 0.0 # Forced outage rate (FOR)
        
        # Remove rows with infinities (occure due to incomplite data or very low number of entries)
        agg_df.replace([np.inf, -np.inf], np.nan, inplace=True) # Replace infinities with NaN
        agg_df = agg_df.dropna() # Drop rows with NaN values
        
        # Reset the index and drop the old index
        agg_df.reset_index(drop=True, inplace=True)
        
        # Save in constructor
        self.agg_df = agg_df
        
        return agg_df
        

    ###############################################################################
    # %% Check Markov properties
    ###############################################################################
    def check_markov(self,elem):
        """
        Check if transition matrix A fullfils desired properties 
           
        Inputs
        ----------
        elem         pandas dataframe that contains the number of transitions, 
                     the time between transitions, generator transition states data, etc.
        
        Outputs
        -------
        A            array containing the transition rates
        empty_states a list containg incomplite state transitions
        """
        
        #__________________________________________________________________________
        # %% Set the transition rate matrix A
        A = np.array([[elem['a00'], elem['a01'], elem['a02'], elem['a03'], elem['a04']],
                       [elem['a10'], elem['a11'], elem['a12'], elem['a13'], elem['a14']],
                       [elem['a20'], elem['a21'], elem['a22'], elem['a23'], elem['a24']],
                       [elem['a30'], elem['a31'], elem['a32'], elem['a33'], elem['a34']], 
                       [elem['a40'], elem['a41'], elem['a42'], elem['a43'], elem['a44']]
                       ])
        
        # %% Count the transitions into and out of the different states    
        # Calculate the "into" and "out of" state transitions for state 1
        in_state_1  = elem['n_01'] + elem['n_21'] + elem['n_31'] + elem['n_41']
        out_state_1 = elem['n_10'] + elem['n_12'] + elem['n_13'] + elem['n_14']
        # Calculate the "into" and "out of" state transitions for state 2
        in_state_2  = elem['n_02'] + elem['n_12'] + elem['n_32'] + elem['n_42']
        out_state_2 = elem['n_20'] + elem['n_21'] + elem['n_23'] + elem['n_24']
        # Calculate the "into" and "out of" state transitions for state 3
        in_state_3  = elem['n_03'] + elem['n_13'] + elem['n_23'] + elem['n_43']
        out_state_3 = elem['n_30'] + elem['n_31'] + elem['n_32'] + elem['n_34']
        # Calculate the "into" and "out of" state transitions for state 4
        in_state_4  = elem['n_04'] + elem['n_14'] + elem['n_24'] + elem['n_34']
        out_state_4 = elem['n_40'] + elem['n_41'] + elem['n_42'] + elem['n_43']
        # NOTE: We want to make sure that if we enter a state, we also exit that 
        #       state, otherwise the Markov chain is not closed and we cannot apply 
        #       the current methodology
        
        # Check if Markov properties are satisfied
        empty_states = []
        if in_state_1 == 0 or out_state_1 == 0:
            A = np.delete(A, 1, 0)
            A = np.delete(A, 1, 1)
            empty_states.append(1)
 
        if in_state_2 == 0 or out_state_2 == 0:
            loc = 2-len(empty_states)
            A = np.delete(A, loc, 0)
            A = np.delete(A, loc, 1)
            empty_states.append(2)

        if in_state_3 == 0 or out_state_3 == 0:
            loc = 3-len(empty_states)
            A = np.delete(A, loc, 0)
            A = np.delete(A, loc, 1)
            empty_states.append(3)

        if in_state_4 == 0 or out_state_4 == 0:
            loc = 4-len(empty_states)
            A = np.delete(A, loc, 0)
            A = np.delete(A, loc, 1)
            empty_states.append(4)
        
        return A, empty_states

        
    ###############################################################################
    # Calculate the steady state probabilities of generators
    ###############################################################################
    def calc_markov(self,A,empty_states):
        """Calculats the steady-state probabilities of a 5-state generator model 
           using continiouse Markov processes.
           In a steady-state we have P*A = 0
           where, 
           P is a vector of steady-state probabilities, P0, P1, P2, P3, P4
                P0 - Available
                P1 - Planned derated
                P2 - Forced derated
                P3 - Planned outage
                P4 - Forced outage
           A is the transition matrix of dimension 5x5
           
           We solve this problem using several different methods
           
        Inputs
        ----------
        A           transtion rate matrix
        
        Outputs
        -------
        P            vector containing the steady-state probabilities
        
        """

        # %% Preparatory functions  
        def is_steady_state(state, A):
            """
            Returns a boolean as to whether a given state is a steady 
            state of the Markov chain corresponding to the matrix A
            """
            return np.allclose((state @ A), 0)


        def adjust_A(A):
            """
            Given the transition rate matrix, prepare it for a algebraic solution
            """
            dim = A.shape[0]
            M = np.vstack((A.transpose()[:-1], np.ones(dim)))
            b = np.vstack((np.zeros((dim - 1, 1)), [1]))
            return M, b


        # %% Implement methods to solve the stead-state continiouse-time Markov

        ## Method 1: Least squares
        def steady_state_least_squares(A):
            """
            Obtain the steady-state as the vector that 
            gives the minimum of a least squares optimisation problem.
            
            Inputs:
            A - the transition rate matrix
            """
            M, b = adjust_A(A)
            pi, _, _, _ = np.linalg.lstsq(M, b, rcond=None)
            return pi.transpose()[0]

        #--------------------------------------------------------------------------
        ## Method 2: Linear algebra
        def steady_state_linear_algebra(A):
            """
            Obtain the steady state vector as the solution of a linear algebraic system.
            
            Inputs:
            A - the transition rate matrix
            """
            M, b = adjust_A(A)
            return np.linalg.solve(M, b).transpose()[0]

        #--------------------------------------------------------------------------
        ## Method 3: 0-eigenvalue
        def steady_state_eigenvectors(A):
            """
            Obtain the steady state vector as the eigenvector that corresponds to the eigenvalue 0.
            
            Inputs:
            A: the transition rate matrix
            """
            
            eigenvalues, eigenvectors = np.linalg.eig(A.T)

            zero_eigenvector = eigenvectors.transpose()[np.argmin(np.abs(eigenvalues))]
            
            P = zero_eigenvector / np.sum(zero_eigenvector)
            
            P = np.asarray(P).flatten()
            
            return P

        #--------------------------------------------------------------------------
        ## Method 4: Solving the defining differential equation analyticaly
        def steady_state_diffeq_analytical(A, max_t=100):
            """
            Solve the defining differential equation analytically until it converges.
            
            Inputs:
            A - the transition rate matrix
            max_t - the maximum time for which the differential equation is solved at each attempt.
            """
            
            # Initialize
            dimension = A.shape[0]
            P         = np.ones((1,dimension)) / dimension
            stateIter = P
            
            while not is_steady_state(P, A):
                P         = P @ sp.linalg.expm(A * max_t)
                stateIter = np.append(stateIter,P,axis=0) 
            
            # Flaten the array
            P = np.asarray(P).flatten()
            
            return P, stateIter

        #--------------------------------------------------------------------------
        ## Method 5: Solving the defining differential equation numerically
        def steady_state_diffeq_numerical(A, max_t = 100, number_of_timepoints = 1000):
            
            """
            Solve the defining differential equation numericaly until it converges.
            
            Inputs:
            A - the transition rate matrix
            max_t - the maximum time for which the differential equation is solved
            number_of_time_points - the number of time points
            """
            
            def dx(x, t):
                return x @ A
            
            dimension = A.shape[0]
            P         = np.ones(dimension) / dimension
            stateIter = P.reshape(1,-1)
            
            while not is_steady_state(P, A):
                ts        = np.linspace(0, max_t, number_of_timepoints)
                xs        = odeint(func=dx, y0=P, t=ts)
                P         = xs[-1]
                tempState = P.reshape(1,-1)
                stateIter = np.append(stateIter,tempState,axis=0)
            
            P = np.asarray(P).flatten()
                
            return P, stateIter


        # %% Solve steady-state Markov
        if steady_type == 1:
            try:
                P = steady_state_least_squares(A)
            except:
                return [-1, -1, -1, -1, -1]    
        elif steady_type == 2:
            try:
                P = steady_state_linear_algebra(A)
            except:
                return [-1, -1, -1, -1, -1]
        elif steady_type == 3:
            try:
                P = steady_state_eigenvectors(A)
                
            except:
                return [-1, -1, -1, -1, -1]
        elif steady_type == 4:
            try:
                P, stateIter = steady_state_diffeq_analytical(A)
                dfDistrIter  = pd.DataFrame(stateIter)
                plt.show(dfDistrIter.plot())
            except:
                return [-1, -1, -1, -1, -1]
        else:
            try:
                P, stateIter = steady_state_diffeq_numerical(A)
                dfDistrIter = pd.DataFrame(stateIter)
                plt.show(dfDistrIter.plot())
            except:
                return [-1, -1, -1, -1, -1]
        
        # Check the empty states and corect the steady-state results (P) vector   
        j = 0
        P_new = np.zeros(5)
        for i in range(5):
            if i not in empty_states:
                P_new[i] = P[i-j]
            else:
                j += 1
            
        return P_new
    
    
###############################################################################        
# %% Execute the class
###############################################################################  
if __name__ == "__main__":
    # Instantiate the class and call the main method
    obj = Continiouse_Markov_Gen()
    obj.calc_steady_state()